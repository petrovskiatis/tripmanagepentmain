<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreTripRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'max_participants' => 'required|integer',
            'price' => 'required|integer',
            'payment_due_date' => 'required',

        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
        public function messages()
        {
            return [
                'name.required' =>   'Name of the trip is required!',
                'start_date.required' => 'Start Date is required!',
                'end_date.required' => 'End Date is required!',
                'max_participants.required' => 'Maximum Participants is required!',
                'price.required' => 'Price is required!',
                'payment_due_date.required' => 'Payment due date is required!'
            ];
        }
}
