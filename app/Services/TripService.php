<?php

namespace App\Services;

use App\Http\Requests\StoreTripRequest;
use App\Models\Trip;
use Illuminate\Http\Request;

class TripService
{
    public function index()
    {
        return Trip::all();
    }

    public function storeTrip(StoreTripRequest $request)
    {
        if(!auth()->user()->tokenCan('agency-permissions')){
            abort(403, 'Unauthorized');
        }

        $validated_fields = $request->validated();

        return Trip::create([
            'name' => $validated_fields['name'],
            'start_date' => $validated_fields['start_date'],
            'end_date' => $validated_fields['end_date'],
            'max_participants' =>$validated_fields['max_participants'],
            'price' => $validated_fields['price'],
            'payment_due_date' => $validated_fields['payment_due_date']
        ]);
    }

    public function updateTrip(Request $request, $id)
    {
        if(!auth()->user()->tokenCan('agency-permissions')){
            abort(403, 'Unauthorized');
        }

        $trip = Trip::findOrFail($id);

        $validated_fields = $request->validate([
            'name' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'max_participants' => 'required',
            'price' => 'required',
            'payment_due_date' => 'required',
        ]);

        $trip->fill([
            'name' => $validated_fields['name'],
            'start_date' => $validated_fields['start_date'],
            'end_date' => $validated_fields['end_date'],
            'max_participants' => $validated_fields['max_participants'],
            'price' => $validated_fields['price'],
            'payment_due_date' => $validated_fields['payment_due_date'],
        ]);

        return $trip->update();
    }

    public function deleteTrip($id)
    {
        if(!auth()->user()->tokenCan('agency-permissions')){
            abort(403, 'Unauthorized');
        }

        $trip = Trip::findOrFail($id);
        return $trip->delete();
    }
}
