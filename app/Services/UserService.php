<?php

namespace App\Services;

use App\Http\Requests\StoreUserRequest;
use App\Models\User;
use Symfony\Component\HttpFoundation\Request;
use Throwable;
use Laravel\Sanctum\HasApiTokens;

class UserService
{
    public function index()
    {
        if(!auth()->user()->tokenCan('agency-permissions')){
            return abort();
        }
        return User::all();
    }

    // we can use Repository for transactions into database for more complex applications
    public function storeUser(StoreUserRequest $request)
    {
        $validated_fields = $request->validated();

        $user = User::create([
            'name' => $validated_fields['name'],
            'email' => $validated_fields['email'],
            'password' => bcrypt($validated_fields['password']),
            'is_agency' => $validated_fields['is_agency'],
        ]);

        ($validated_fields['is_agency']) ? $token = $user->createToken('create-user', ['agency-permissions']) : $token = $user->createToken('create-user', ['user-permissions']);

        $response = [
            'user' => $user,
            'token' => $token
        ];

        return response($response, 201);
    }

    public function updateUser(Request $request, $id)
    {
//        dd($id !== (string) auth()->user()->id);
        $authenticated_user = (string) auth()->user()->id;
        if($id !== $authenticated_user){
            abort(403, 'Cannot update  user');
        }

        $user = User::findOrFail($id);

        $fields = $request->validate([
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'is_agency' => ''
        ]);

        $user->fill([
            'name' => $fields['name'],
            'email' =>   $fields['email'],
            'password' => bcrypt($fields['password']),
            'is_agency' => $fields['is_agency']
        ]);

        return $user->save();
    }

    public function deleteUser($id)
    {
        if(!auth()->user()->tokenCan('agency_permissions')){
            abort(403, 'You do not have permissions to delete users');
        }

        $user = User::findOrFail($id);
        return $user->delete();
    }
}
