<?php

namespace App\Services;

use App\Http\Requests\CreateReviewRequest;
use App\Models\Review;

class ReviewService
{
    public function index()
    {
        return Review::where(['user_id' => auth()->user()->id]);
    }

    public function createReview($request)
    {
        if(!auth()->user()->tokenCan('user-permissions')){
            abort(403, 'Only users can create Reviews for trips');
        }

        $validated_fields = $request->validated($request);

        return Review::create([
            'text' => $validated_fields['text'],
            'trip_id' => $validated_fields['trip_id'],
            'user_id' => auth()->user()->id
        ]);
    }

    public function updateReview($request, $id)
    {
        if(!auth()->user()->tokenCan('user-permissions')){
            abort(403, 'Only users can update Reviews for trips');
        }

        $validated_fields = $request->validate([
            'text' => 'required|string',
        ]);

        $review = Review::findOrFail($id);

        $review->fill([
            'text' => $validated_fields['text']
        ]);

        return $review->update();
    }

    public function deleteReview($id)
    {
        if(!auth()->user()->tokenCan('user-permissions')){
            abort(403, 'Only users can delete Reviews for trips');
        }

        $review = Review::findOrFail($id);
        return $review->delete();
    }
}
