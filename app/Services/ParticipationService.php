<?php

namespace App\Services;

use App\Http\Requests\ParticipationStoreRequest;
use App\Models\Participation;
use App\Models\Trip;
use Symfony\Component\HttpFoundation\Request;
use App\Models\User;

class ParticipationService
{
    public function index()
    {
        return Participation::all();
    }
    public function storeParticipation(ParticipationStoreRequest $request)
    {
        if(!auth()->user()->tokenCan('user-permissions')){
            abort(403, 'Only users can participate on trips');
        }

        $this->checkIfUserExists($request);
        $this->checkIfTripExists($request);

        $validated_fields = $request->validated($request);

        return Participation::create([
            'user_id' =>  $validated_fields['user_id'],
            'trip_id' => $validated_fields['trip_id'],
            'paid_on_date' => $validated_fields['paid_on_date']
        ]);
    }

    public function updateParticipation(Request $request, Participation $participation)
    {
        if(!auth()->user()->tokenCan('user-permissions')){
            abort(403, 'Only users can edit participations');
        }

        $this->checkIfUserExists($request);
        $this->checkIfTripExists($request);

        $update_participation = Participation::findOrFail($participation->id);

        $validated_fields = $request->validate([
            'user_id' => 'required',
            'trip_id' => 'required',
            'paid_on_date' => 'nullable'
        ]);

        $update_participation->fill([
            'user_id' =>  $validated_fields['user_id'],
            'trip_id' => $validated_fields['trip_id'],
            'paid_on_date' => $validated_fields['paid_on_date']
        ]);

        $update_participation->save();
    }

    public function deleteParticipation($participation)
    {
        if(!auth()->user()->tokenCan('user-permissions')){
            abort(403, 'Only users can edit participations');
        }

        $participation_to_delete = Participation::findOrFail($participation->id);
        return $participation_to_delete->delete();
    }

    private function checkIfUserExists($request)
    {
        if( !User::where(['id' => $request->all('user_id')])->first()){
            abort(404, 'Wrong User Id!');
        }
    }

    private function checkIfTripExists($request)
    {
        if( !Trip::where(['id' => $request->all('trip_id')])->first()){
            abort(404, 'Wrong Trip Id!');
        }
    }
}
